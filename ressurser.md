# Resurser for utvikelere som utvikler Fri programvare (Under utvikling)


## Fri programvare

- [The Apache Project Maturity Model](https://community.apache.org/apache-way/apache-project-maturity-model.html) - A suggested framework for evaluating the overall maturity of an Apache project community and the codebase that it maintains. While this model is specific to Apache projects many of these factors are important for any open source project.
- The [Open Source Guides](https://opensource.guide/) are a collection of resources for individuals, communities, and companies who want to learn how to run and contribute to an open source project.(specific for and by Github, but many parts can be re-mixed and re-used for more general purpose)
- [The Standard for Public Code](https://github.com/publiccodenet/standard/blob/develop/introduction.md) is a set of criteria that supports public organizations in developing and maintaining software and policy together. The Standard describes a number of criteria. All criteria have consistent sections that make it clear how to create great public code. Below is a brief explanation of each of these sections and how they are used within the criteria of the Standard.
- [The Linux Foundation Open Source Guides For The Enterprise](https://www.linuxfoundation.org/resources/open-source-guides/). Leverage best practices for running an open source program office or starting an open source project in your organization. Developed by The Linux Foundation in partnership with the TODO Group, these resources represent the experience of our staff, projects, and members.
- [A primer developed by the Digital Impact and Governance Initiative](https://www.newamerica.org/digital-impact-governance-initiative/reports/building-and-reusing-open-source-tools-government/) for any government entity thinking about embracing open source solutions. Open source tools have been around since the 1980s, and used by governments for decades. As a result, there are known methods for making the most of using the resource.
- [A manual from Gov.uk - Making source code open and reusable](https://www.gov.uk/service-manual/technology/making-source-code-open-and-reusable) When you create new source code, you must make it open so that other developers (including those outside government) can benefit from your work and build on it, learn from your experiences and find uses for your code which you hadn’t found.
- [The OWASP Foundation](https://owasp.org/) works to improve the security of software through its community-led open source software projects, hundreds of chapters worldwide, tens of thousands of members, and by hosting local and global conferences.

## Åpne data 

[The Open Data Handbook](http://opendatahandbook.org/guide/en/) discusses the legal, social and technical aspects of open data. It can be used by anyone but is especially designed for those seeking to open up data. It discusses the why, what and how of open data – why to go open, what open is, and the how to &#39;open&#39; data.

[Publishing Open Government Data](https://www.w3.org/TR/gov-data/) Is designed to help governments open and share their data, the W3C eGov Interest Group has developed the following guidelines. These straightforward steps emphasize standards and methodologies to encourage publication of government data, allowing the public to use this data in new and innovative ways.

[The Dublin Core Metadata Initiative](https://dublincore.org/) supports innovation in metadata design and best practices. DCMI is supported by its members and is a project of ASIS&T.

A Data Protection Impact Assessment (DPIA) is a way for you to systematically and comprehensively analyse your processing and help you identify and minimise data protection risks. DPIAs should consider compliance risks, but also broader risks to the rights and freedoms of individuals, including the potential for any significant social or economic disadvantage. The focus is on the potential for harm – to individuals or to society at large, whether it is physical, material or non-material. To assess the level of risk, a DPIA must consider both the likelihood and the severity of any impact on individuals.

Data protection impact assessments are required under GDPR but in general are a good idea to require for any humanitarian technology project using data. Most EU countries will have a web page with resources on how to conduct one. For reference, refer to [the one from the UK](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/accountability-and-governance/data-protection-impact-assessments/) which includes a document template that you just fill out.

## Åpen forskning (Open access)
[Guidance on the Implementation of Plan S](https://www.coalition-s.org/guidance-on-the-implementation-of-plan-s/). Plan S aims for full and immediate Open Access to peer-reviewed scholarly publications from research funded by public and private grants. cOAlition S, the coalition of research funders that have committed to implementing Plan S, aims to accelerate the transition to a scholarly publishing system that is characterised by immediate, free online access to, and largely unrestricted use and re-use (full Open Access) of scholarly publications. Bill & Melinda Gates Foundation is one of many organizations that has changed their open access policy effective January 1, 2021 to align with Plan S principles.

## Fri Kultur


### General resources for Creative Commons licenses
- [The Creative Commons Legal Database](https://creativecommons.org/2020/12/03/explore-the-new-cc-legal-database-site/) is a collection of case law and legal scholarship to help our users learn more about legal issues surrounding Creative Commons (CC) licenses and legal tools. This information has been contributed by many dedicated members of the CC Global Network and the CC Legal Team. 

- [The Creative Commons licenses](https://creativecommons.org/about/cclicenses/) give everyone from individual creators to large institutions a standardized way to grant the public permission to use their creative work under copyright law. This page provides basic inforamtion and guidlines for picking licenses based on spesific user or project goals. 


### Fri digitale læringsressurser

- [The Open Education Handbook](https://en.wikibooks.org/wiki/Open_Education_Handbook). &quot;Open Education&quot; is a topic which has become increasingly popular in a variety of contexts. This handbook has been written to provide a useful point of reference for readers with a range of different roles and interests who are interested in learning more about the concept of Open Education and to help them deal with a variety of practical situations.

- Creative Commons Certificate for Educators and Librarians. Unless otherwise noted, all CC Certificate content is licensed under the Creative Commons Attribution 4.0 International license (CC BY). Accessing this CC BY content is not a substitute for enrolling in the official course
https://certificates.creativecommons.org/about/certificate-resources-cc-by/

- [Guidelines for open educational resources (OER) in higher education](http://www.unesco.org/new/en/communication-and-information/resources/publications-and-communication-materials/publications/full-list/guidelines-for-open-educational-resources-oer-in-higher-education/). These Guidelines outline key issues and make suggestions for integrating OER into higher education. Their purpose is to encourage decision makers in governments and institutions to invest in the systematic production, adaptation and use of OER and to bring them into the mainstream of higher education in order to improve the quality of curricula and teaching and to reduce costs.

## Beste praksis 


### Lover og regler
[An introducation](https://opensource.guide/legal/) to the legal implications of open source from Open Source Guides and GitHub. 


### Universel utformin

The [WCAG 2.0 Guidelines](https://www.w3.org/WAI/standards-guidelines/wcag/) covers a wide range of recommendations for making web content more accessible. 


### Beste praksis og prinsipper

- [The Development Principles](https://digitalprinciples.org/) - https://digitalprinciples.org/ - A set of living guidance intended to help practitioners succeed in applying digital technologies to development programs.





